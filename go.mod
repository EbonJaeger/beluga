module github.com/EbonJaeger/beluga

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/DataDrake/waterlog v1.0.5
	github.com/bwmarrin/discordgo v0.20.2
	github.com/gorilla/websocket v1.4.1 // indirect
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17 // indirect
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
)
